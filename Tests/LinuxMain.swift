import XCTest

import SnapshotTests

var tests = [XCTestCaseEntry]()
tests += SnapshotTests.allTests()
XCTMain(tests)
