import XCTest
import GivenWhenThen
@testable import Snapshot

final class UIWindowCurrentTests: XCTestCase {

	#if os(iOS)
	/// Tests whether `UIWindow.current(on:)` works as expected.
	func testCurrentWindow() throws {
		try feature("testCurrentWindow") { [self] in
			scenario("Current Window is `nil`") {
				given("A setup where there is no active window") { _ in
					UIWindow.current()
				}
				.then("`UIWindow.current()` returns `nil`") {
					expect(isNil: $0)
				}
			}

			scenario("Current Window is not `nil`") {
				given("A setup where there is an active window") { _ in
					sceneContainerWithWindow()
				}
				.then("`UIWindow.current()` returns the key window") { (sceneContainer, window) in
					expect(UIWindow.current(on: sceneContainer), isEqual: window)
				}
			}
		}
	}

	// MARK: - All Tests

	static var allTests = [
		("testCurrentWindow", testCurrentWindow)
	]
	#else
	static var allTests = [(String, (UIWindowCurrentTests) -> () throws -> ())]()
	#endif
}

#if os(iOS)
// MARK: - Private

extension XCTestCase {
	func sceneContainerWithWindow() -> (sceneContainer: SceneContainer, window: UIWindow) {
		let window = UIWindow()
		let scene = MockedScene()
		scene.window = window
		let sceneContainer = MockedSceneContainer()
		sceneContainer.activeScenes.append(scene)
		return (sceneContainer, window)
	}
}

private final class MockedScene: NSObject, SceneDelegateContainer, UIWindowSceneDelegate {
	weak var delegate: UISceneDelegate?
	var window: UIWindow?

	override init() {
		super.init()
		self.delegate = self
	}
}

private final class MockedSceneContainer: SceneContainer {
	var activeScenes: [SceneDelegateContainer] = []
}
#endif
