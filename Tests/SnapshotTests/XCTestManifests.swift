import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(XCTestCaseSnapshotTests.allTests),
		testCase(UIWindow+CurrentTests.allTests)
    ]
}
#endif
