import XCTest
import GivenWhenThen
@testable import Snapshot

final class XCTestCaseSnapshotTests: XCTestCase {

	#if os(iOS)
	/// Tests whether `XCTestCase.takeSnapshot(for:from:)` works as expected.
	func testSnapshotEquality() throws {
		let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
		let view = UIView(frame: frame)
		view.backgroundColor = .white

		try feature("testSnapshotEquality") { [self] in
			scenario("Snapshot equality") {
				given("Two snapshots taken from the same `UIView`",
					   initialValue: view) { view in
					(takeSnapshot(view), takeSnapshot(view))
				}
				.then("they are considered equal using the `==` operator") { (snapshot1, snapshot2) in
					expect(snapshot1, isEqual: snapshot2)
				}
				.and("their difference is equal to either view") { (snapshot1, snapshot2) in
					let difference = snapshot1.difference(from: snapshot2)
					expect(difference, isEqual: snapshot1)
					expect(difference, isEqual: snapshot2)
				}
			}
		}
	}

	/// Tests whether `Snapshot.difference(from:)` works as expected.
	func testSnapshotDifference() throws {
		let frame100 = CGRect(x: 0, y: 0, width: 100, height: 100)
		let frame200 = CGRect(x: 0, y: 0, width: 200, height: 200)
		let view1 = UIView(frame: frame100)
		view1.backgroundColor = .orange
		let view2 = UIView(frame: frame100)
		view2.backgroundColor = .purple
		let view3 = UIView(frame: frame200)

		try feature("testSnapshotDifference") { [self] in
			scenarioOutline("Snapshot difference (same size / different size)") { initialValue in
				given("Two snapshots from different `UIView` objets",
					   initialValue: initialValue) { (view1, view2) -> (Snapshot, Snapshot) in
					(takeSnapshot(view1), takeSnapshot(view2))
				}
				.then("they are not considered equal using the `==` operator") { (snapshot1, snapshot2) in
					expect(snapshot1, isNotEqual: snapshot2)
				}
				.and("their difference is not equal to either view") { (snapshot1, snapshot2) in
					let difference = snapshot1.difference(from: snapshot2)
					expect(difference, isNotEqual: snapshot1)
					expect(difference, isNotEqual: snapshot2)
				}
			}
			examples: {
				[(view1, view2),
				 (view1, view3)]
			}
		}
	}

	/// Tests whether `snapshot_referencePath` is set.
	func testReferencePathSet() throws {
		try feature("testReferencePathSet") { [self] in
			scenario("Environment variable `snapshot_referencePath` is present") {
				given("`snapshot_referencePath` is an environment variable") { _ in
					ProcessInfo[Snapshot.Variables.referencePath]
				}
				.then("this variable is active and has a value") { referencePath in
					expect(isNotNil: referencePath)
				}
			}
		}
	}

	/// Tests whether `Snapshot.init(_:)` (referenceSnapshotName) is set.
	func testInitWithReferenceSnapshotName() throws {
		try feature("testInitWithReferenceSnapshotName") { [self] in
			scenario("`Snapshot.init(_:)` (referenceSnapshotName) failure") {
				given("a random reference path name that does not point to a reference snapshot") { _ in
					URL(fileURLWithPath: UUID().uuidString)
				}
				.when("passing this name to `Snapshot.init(_:)`") { referenceSnapshotImages in
					Snapshot(referenceSnapshotImages)
				}
				.then("the result is `nil`") { snapshot in
					expect(isNil: snapshot)
				}
			}

			try scenario("`Snapshot.init(_:)` (referenceSnapshotName) success") {
				try given("a reference path name that points to a reference snapshot") { _ in
					let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
					let view = UIView(frame: frame)
					view.backgroundColor = .white
					let name = UUID().uuidString
					try storeReferenceSnapshot(view, name: name)
					let previousReferencePath = ProcessInfo[Snapshot.Variables.referencePath]
					setSnapshotReferencePath(NSTemporaryDirectory())
					let referenceSnapshotURL = snapshotURL(name)
					setSnapshotReferencePath(previousReferencePath)
					return referenceSnapshotURL
				}
				.when("passing this name to `Snapshot.init(_:)`") { referenceSnapshotURL in
					Snapshot(referenceSnapshotURL)
				}
				.then("the result is a valid `Snapshot` object") { snapshot in
					expect(isNotNil: snapshot)
				}
			}
		}
	}

	/// Tests whether `XCTestCase.snapshotURL(_:)` works as expected.
	func testSnapshotURL() throws {
		try feature("testSnapshotURL") { [self] in
			scenario("`snapshotURL(_:)` with `snapshot_referencePath` not set") {
				given("`snapshot_referencePath` is not set", initialValue: ProcessInfo[Snapshot.Variables.referencePath]) { originalReferencePath in
					setSnapshotReferencePath(nil)
				}
				.when("`snapshotURL(_:)` is triggered") { originalReferencePath in
					(snapshotURL(), originalReferencePath)
				}
				.then("the result has `/` prefix") { (url, originalReferencePath) in
					expect(isTrue: url.path.hasPrefix("/"))
					setSnapshotReferencePath(originalReferencePath)
				}
			}

			scenario("`snapshotURL(_:)` with `snapshot_referencePath` set") {
				given("`snapshot_referencePath` is not set", initialValue: ProcessInfo[Snapshot.Variables.referencePath]) { originalReferencePath in
					setSnapshotReferencePath(NSTemporaryDirectory())
				}
				.when("`snapshotURL(_:)` is triggered") { originalReferencePath in
					(snapshotURL(), originalReferencePath)
				}
				.then("the result has `/` prefix") { (url, originalReferencePath) in
					expect(isTrue: url.path.hasPrefix("/"))
					setSnapshotReferencePath(originalReferencePath)
				}
			}
		}
	}

	/// Tests whether `XCTestCase.snapshotTest(_:name:)` works as expected.
	func testSnapshotTest() throws {
		try feature("testSnapshotTest") { [self] in
			try scenario("Snapshot Test") {
				try given("a view", initialValue: UIView()) { view in
					view.backgroundColor = .orange
					view.frame = CGRect(origin: .zero, size: CGSize(width: 100, height: 100))
				}
				.then("`snapshotTest` works as expected") { view in
					let failOnMissing = ProcessInfo[Snapshot.Variables.failOnMissingReference]
					setFailsOnMissingSnapshotReference(false)
					let referencePath = ProcessInfo[Snapshot.Variables.referencePath]
					setSnapshotReferencePath(NSTemporaryDirectory())
					try snapshotTest(view, name: "test")
					setSnapshotReferencePath(referencePath)
					setFailsOnMissingSnapshotReference(failOnMissing != nil)
				}
			}
		}
	}

	// MARK: - Snapshot Reference

	/// Tests whether `XCTestCase.expect(matchesSnapshotReference:named:_:file:line:)`
	/// works as expected.
	func testSnapshotReference() throws {
		let frame = CGRect(x: 0, y: 0, width: 100, height: 100)
		let view = UIView(frame: frame)
		view.backgroundColor = .white

		typealias Passthrough1 = (view: UIView, failOnMissing: String?)
		typealias Passthrough2 = (view: UIView, name: String, failOnMissing: String?, path: String?)

		try feature("testIsGreaterThanOrEqual") { [self] in
			try scenario("Missing snapshot reference -> Fail") {
				try given("A view", initialValue: view) { view -> Passthrough1 in
					Passthrough1(view: view,
								 failOnMissing: ProcessInfo[Snapshot.Variables.failOnMissingReference])
				}
				.when("`failOnMissingReference` environment variable is set") { _ in
					setFailsOnMissingSnapshotReference(true)
				}
				.then("`expect(matchesSnapshotReference:named:)` will throw") { passthrough in
					try expect(throws: expect(matchesSnapshotReference: passthrough.view))
					setFailsOnMissingSnapshotReference(passthrough.failOnMissing != nil)
				}
			}

			try scenario("Missing snapshot reference -> Create") {
				try given("A view", initialValue: view) { view -> Passthrough2 in
					Passthrough2(view, UUID().uuidString, ProcessInfo[Snapshot.Variables.failOnMissingReference], ProcessInfo[Snapshot.Variables.referencePath])
				}
				.when("`failOnMissingReference` environment variable is not set") { _ in
					setFailsOnMissingSnapshotReference(false)
				}
				.then("`expect(matchesSnapshotReference:named:)` will pass") { passthrough in
					setSnapshotReferencePath(NSTemporaryDirectory())
					try expect(matchesSnapshotReference: passthrough.view, named: passthrough.name)
					setSnapshotReferencePath(passthrough.path)
					setFailsOnMissingSnapshotReference(passthrough.failOnMissing != nil)
				}
			}

			try scenario("Found snapshot reference matching") {
				try given("A view", initialValue: view) { view in
					Passthrough2(view, UUID().uuidString, ProcessInfo[Snapshot.Variables.failOnMissingReference], ProcessInfo[Snapshot.Variables.referencePath])
				}
				.and("a reference snapshot of the view is available") { passthrough in
					try storeReferenceSnapshot(passthrough.view, name: passthrough.name)
				}
				.when("`failOnMissingReference` environment variable is not set") { _ in
					setFailsOnMissingSnapshotReference(false)
				}
				.then("`expect(matchesSnapshotReference:named:)` will pass") { passthrough in
					setSnapshotReferencePath(NSTemporaryDirectory())
					try expect(matchesSnapshotReference: passthrough.view, named: passthrough.name)
					setSnapshotReferencePath(passthrough.path)
					setFailsOnMissingSnapshotReference(passthrough.failOnMissing != nil)
				}
			}

			try scenario("Found snapshot reference not matching") {
				try given("A view", initialValue: view) { view in
					Passthrough2(view: view,
								 name: UUID().uuidString,
								 failOnMissing: ProcessInfo[Snapshot.Variables.failOnMissingReference],
								 path: ProcessInfo[Snapshot.Variables.referencePath])
				}
				.and("a reference snapshot of the view is available") { passthrough in
					try storeReferenceSnapshot(passthrough.view, name: passthrough.name)
				}
				.but("the reference snapshot does not match the current view") { _ in
					view.backgroundColor = .orange
				}
				.when("`failOnMissingReference` environment variable is not set") { _ in
					setFailsOnMissingSnapshotReference(false)
				}
				.then("`expect(matchesSnapshotReference:named:)` will throw") { passthrough in
					setSnapshotReferencePath(NSTemporaryDirectory())
					try expect(throws: expect(matchesSnapshotReference: passthrough.view, named: passthrough.name))
					setSnapshotReferencePath(passthrough.path)
					setFailsOnMissingSnapshotReference(passthrough.failOnMissing != nil)
				}
			}
		}
	}

	// MARK: - All Tests

	static var allTests = [
		("testSnapshotEquality", testSnapshotEquality),
		("testSnapshotDifference", testSnapshotDifference),
		("testReferencePathSet", testReferencePathSet),
		("testInitWithReferenceSnapshotName", testInitWithReferenceSnapshotName),
		("testSnapshotURL", testSnapshotURL),
		("testSnapshotTest", testSnapshotTest),
		("testSnapshotReference", testSnapshotReference)
	]
	#else
	static var allTests = [(String, (XCTestCaseSnapshotTests) -> () throws -> ())]()
	#endif
}
