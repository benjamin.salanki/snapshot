//
//  XCTestCase+Snapshot.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 12.10.20.
//

import Foundation
import XCTest
import GivenWhenThen

#if os(iOS)
public extension XCTestCase {
	/// Takes a `Snapshot` from the passed `SnapshotProvider` and adds it as an `XCTAttachment`.
	/// - Parameters:
	///   - snapshotProvider: The `SnapshotProvider` to query for a snapshot.
	///   - name: The name of the snapshot.
	/// - Returns: The created `Snapshot`.
	@discardableResult
	func takeSnapshot(_ snapshotProvider: SnapshotProvider, name: String = "Default") -> Snapshot {
		var snapshot = snapshotProvider.snapshot()
		snapshot.name = name
		add(XCTAttachment.snapshot(snapshot))
		return snapshot
	}

	/// Tests if the reference snapshot for the passed view matches the view.
	/// - Parameters:
	///   - view: The view to test.
	///   - name: The name of the view when storing the snapshot.
	func snapshotTest(_ view: UIView, name: String) throws {
		try feature("snapshotTest") { [self] in
			try scenario("\(name) snapshot") {
				try given("a view of type \(type(of: view))", initialValue: view)
					.then("its snapshot matches its reference snapshot") { view in
						try expect(matchesSnapshotReference: view, named: name, name)
					}
			}
		}
	}

	/// File `URL` with a path to an `.imageset` with a supplied name.
	///
	/// The `URL`'s path is constructed by using `Snapshot.Variables.referencePath`
	/// as the root and appending the test name, the device name and finally `name`.
	/// - Parameter name: The name of the `.imageset` the `URL` points to.
	/// - Returns: a file `URL` with a path to `.../name.imageset`.
	func snapshotURL(_ name: String = "Default") -> URL {
		let deviceName = UIDevice
			.current
			.name
			.replacingOccurrences(of: ".", with: ",")
		let testName = self.name
			.trimmingCharacters(in: .init(charactersIn: "-+[]"))
			.replacingOccurrences(of: " ", with: "/")
		var root = ProcessInfo[Snapshot.Variables.referencePath] ?? ""
		if !root.hasSuffix("/") {
			root += "/"
		}
		let name = name
			.replacingOccurrences(of: "/", with: "-")
			.replacingOccurrences(of: ":", with: "-")
		return URL(fileURLWithPath: "\(root)\(testName)/\(deviceName)/\(name).imageset")
	}
}
#endif

extension XCTestCase {

	/// Writes a snapshot of a view to `NSTemporaryDirectory`.
	/// - Parameters:
	///   - view: The view to create a snapshot of.
	///   - name: The name for the snapshot.
	/// - Throws: Errors if storing the snapshot fails.
	func storeReferenceSnapshot(_ view: UIView, name: String) throws {
		let previousReferencePath = ProcessInfo[Snapshot.Variables.referencePath]
		setSnapshotReferencePath(NSTemporaryDirectory())
		try view.referenceSnapshot(self, name: name)
		setSnapshotReferencePath(previousReferencePath)
	}

	/// Sets or unsets `snapshot_referencePath` environment variable.
	/// - Parameter path: The path to set. If set to `nil`, unsets the variable.
	func setSnapshotReferencePath(_ path: String?) {
		let variable = Snapshot.Variables.referencePath.rawValue
		guard let path = path else {
			unsetenv(variable)
			return
		}
		setenv(variable, path, 1)
	}

	/// Sets or unsets `snapshot_referencePath` environment variable.
	/// - Parameter path: The path to set. If set to `nil`, unsets the variable.
	func setFailsOnMissingSnapshotReference(_ boolValue: Bool) {
		let variable = Snapshot.Variables.failOnMissingReference.rawValue
		if boolValue {
			setenv(variable, String(boolValue), 1)
		} else {
			unsetenv(variable)
		}
	}
}

#if os(iOS)
extension XCTestCase {
	// MARK: - Snapshot

	/// Asserts that a snapshot of the passed view matches its reference snapshot.
	/// - Parameters:
	///   - view: The view to validate.
	///   - name: A name to use for the snapshot. Useful when expecting more than
	///   one snapshot per test case. Shows in the test reports.
	///   - message: An optional description of a failure.
	///   - file: The file in which the failure occurred. The default is the file
	///   name of the test case in which this method was called.
	///   - line: The line number on which the failure occurred. The default is
	///   the line number on which this method was called.
	/// - Throws:
	/// 	- `Snapshot.Errors.referenceMissing(String)` if a reference snapshot
	/// 	could not be located for the view and the `snapshot_failOnMissingReference`
	/// 	environment variable is set.
	///  	- `Snapshot.Errors.snapshotDoesNotMatchReference` if the reference
	///  	does not match the snapshot taken of `view`.
	/// - Returns: The `view` parameter.
	@discardableResult
	func expect(matchesSnapshotReference view: @autoclosure () throws -> UIView, named name: @autoclosure () throws -> String = "Default", _ message: @autoclosure () -> String = "", file: StaticString = #filePath, line: UInt = #line) throws -> UIView {
		let failOnMissingReference = (ProcessInfo[Snapshot.Variables.failOnMissingReference] as NSString?)?.boolValue ?? false
		let view = try view()
		let name = try name()
		let message = message()
		let referencePath = snapshotURL(name)
		var reference: Snapshot! = Snapshot(referencePath)
		if var reference = reference {
			reference.name = name + "-reference"
			add(XCTAttachment.snapshot(reference))
		} else {
			if failOnMissingReference {
				throw Snapshot.Errors.referenceMissing(referencePath.path)
			} else {
				reference = try view.referenceSnapshot(self, name: name)
				reference?.name = name + "-reference"
			}
		}
		let snapshot = takeSnapshot(view, name: "\(name)-current")
		var difference = snapshot.difference(from: reference)
		difference.name = "\(name)-difference"
		add(XCTAttachment.snapshot(difference))
		if difference != reference {
			throw Snapshot.Errors.snapshotDoesNotMatchReference(message)
		}
		return view
	}
}
#endif
