//
//  UIImage+Snapshot.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 18.10.20.
//

#if os(iOS)
import UIKit

extension UIImage {
	/// Creates an `UIImage` from a `CIImage` with the passed size and scale.
	/// - Parameters:
	///   - ciImage: The image to draw.
	///   - size: The size to draw the image at.
	///   - scale: The scale to use for the returned `UIImage`.
	/// - Returns: An `UIImage` extracted from the context the image is drawn in.
	static func ciImage(_ ciImage: CIImage, size: CGSize, scale: CGFloat) -> UIImage {
		UIGraphicsBeginImageContextWithOptions(size, true, scale)
		defer { UIGraphicsEndImageContext() }
		let cgContext = UIGraphicsGetCurrentContext()!
		let context = CIContext(cgContext: cgContext, options: [.outputColorSpace: cgContext.colorSpace!])
		let cgOutputImage = context.createCGImage(ciImage, from: ciImage.extent)!
		let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: size.height)
		cgContext.concatenate(flipVertical)
		cgContext.draw(cgOutputImage, in: CGRect(origin: .zero, size: size))
		return UIGraphicsGetImageFromCurrentImageContext()!
	}

	/// Set of `UIImage`s pointing to the light and dark mode variants of a
	/// snapshot located at the passed `URL`.
	/// - Parameter referenceSnapshotURL: The `URL` of a stored reference snapshot's
	/// `.imageset` folder.
	/// - Returns: A tuple of a light and dark mode `UIImage`s loaded from the `.imageset`
	/// folder at `referenceSnapshotURL`.
	static func referenceSnapshotImages(_ referenceSnapshotURL: URL) -> (light: UIImage, dark: UIImage)? {
		let lightPath = referenceSnapshotURL.appendingPathComponent("Default.png")
		let darkPath = referenceSnapshotURL.appendingPathComponent("Dark.png")
		guard let lightImage = try? UIImage(data: Data.init(contentsOf: lightPath)),
			  let darkImage = try? UIImage(data: Data.init(contentsOf: darkPath)) else {
			return nil
		}
		return (lightImage, darkImage)
	}

	/// An image from the difference of two `Snapshot`s' images extracted from `keyPath`.
	/// - Parameters:
	///   - snapshot1: The first `Snapshot`.
	///   - snapshot2: The second `Snapshot`.
	///   - keyPath: The key path (`\.lightImage` or `\.darkImage`) to use when extracting the snapshot image.
	/// - Returns: An `UIImage` generated from the difference of the passed `Snapshot`s' images at `keyPath`.
	static func snapshotDifference(_ snapshot1: Snapshot, _ snapshot2: Snapshot, keyPath: KeyPath<Snapshot, UIImage>) -> UIImage {
		let image1 = CIImage(cgImage: snapshot1[keyPath: keyPath].cgImage!)
		let image2 = CIImage(cgImage: snapshot2[keyPath: keyPath].cgImage!)
		return UIImage.ciImage(image1.difference(from: image2),
							   size: snapshot1[keyPath: keyPath].size.combining(snapshot2[keyPath: keyPath].size),
							   scale: snapshot1[keyPath: keyPath].scale)

	}
}
#endif
