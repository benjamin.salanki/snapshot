//
//  UIView+SnapshotProvider.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 11.10.20.
//

#if os(iOS)
import UIKit

extension UIView: SnapshotProvider {
	public func snapshot() -> Snapshot {
		Snapshot(self)
	}
}
#endif
