//
//  UIView+Image.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 18.10.20.
//

#if os(iOS)
import UIKit

extension UIView {
	/// Creates a tuple of `UIImage`s from the receiver, one of light and one of dark mode.
	func images() -> (light: UIImage, dark: UIImage) {
		let window = UIWindow.current()
		let previousSuperview = superview
		if window != self {
			window?.addSubview(self)
		}
		var images: [UIImage] = []
		[UIUserInterfaceStyle.light, .dark].forEach { interfaceStyle in
			window?.overrideUserInterfaceStyle = interfaceStyle
			images.append(image())
		}
		window?.overrideUserInterfaceStyle = .unspecified
		previousSuperview?.addSubview(self)
		return (images[0], images[1])
	}

	/// Creates a `UIImage` from the receiver.
	func image() -> UIImage {
		UIGraphicsBeginImageContextWithOptions(bounds.size, isOpaque, 0.0)
		defer { UIGraphicsEndImageContext() }
		layer.render(in: UIGraphicsGetCurrentContext()!)
		let renderedImage = UIGraphicsGetImageFromCurrentImageContext()!
		var image = renderedImage
		let temporaryFileName = "\(UUID().uuidString).png"
		let temporaryPath = URL(fileURLWithPath: NSTemporaryDirectory().appending(temporaryFileName))
		try? renderedImage.pngData()?.write(to: temporaryPath)
		if let loadedImage = try? UIImage(data: Data(contentsOf: temporaryPath)) {
			image = loadedImage
		}
		try? FileManager.default.removeItem(at: temporaryPath)
		return image
	}
}

#endif
