//
//  ProcessInfo+EnvironmentVariables.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 16.10.20.
//

import Foundation

public protocol EnvironmentVariable {
	var rawValue: String { get }
}

extension ProcessInfo {
	static subscript(environmentVariable: EnvironmentVariable) -> String? {
		Self.processInfo.environment[environmentVariable.rawValue]
	}
}
