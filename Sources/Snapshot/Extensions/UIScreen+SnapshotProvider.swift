//
//  UIScreen+SnapshotProvider.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 29.11.20.
//

import Foundation

#if os(iOS)
import UIKit

extension UIScreen: SnapshotProvider {
	public func snapshot() -> Snapshot {
		Snapshot(snapshotView(afterScreenUpdates: true))
	}
}
#endif
