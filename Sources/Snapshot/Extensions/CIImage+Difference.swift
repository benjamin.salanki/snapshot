//
//  CIImage+Difference.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 18.10.20.
//

import CoreImage

extension CIImage {
	/// Creates an image from the difference of the passed image and the receiver.
	///
	/// Uses `CIDifferenceBlendMode` filter to calculate the difference.
	/// - Parameters:
	///   - image: The image to use for calculation.
	/// - Returns: A `CIImage` from the difference of `image` and `self`.
	func difference(from image: CIImage) -> CIImage {
		let differenceFilter = CIFilter(name:"CIDifferenceBlendMode")!
		differenceFilter.setValue(image, forKey: kCIInputImageKey)
		differenceFilter.setValue(self, forKey: kCIInputBackgroundImageKey)
		return differenceFilter.outputImage!
	}
}
