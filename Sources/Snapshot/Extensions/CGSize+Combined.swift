//
//  CGSize+Combined.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 18.10.20.
//

import CoreGraphics

extension CGSize {
	/// Creates a `CGSize` that fits the receiver and the passed size.
	///
	/// - Parameters:
	///   - size: The other size.
	/// - Returns: A `CGSize` that is just large enough to fit `self` and `size` if overlaid.
	func combining(_ size: CGSize) -> CGSize {
		CGSize(width: max(width, size.width),
			   height: max(height, size.height))
	}
}
