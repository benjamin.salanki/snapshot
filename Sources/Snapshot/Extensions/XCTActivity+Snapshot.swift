//
//  XCTActivity+Snapshot.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 18.10.20.
//

import XCTest

extension XCTActivity {
	/// Associates two files, images, or other attachments with the activity.
	/// - Parameter attachments: A tuple of `XCTAttachment`s.
	func add(_ attachments: (XCTAttachment, XCTAttachment)) {
		add(attachments.0)
		add(attachments.1)
	}
}
