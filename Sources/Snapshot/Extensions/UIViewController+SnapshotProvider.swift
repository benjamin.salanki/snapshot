//
//  UIViewController+SnapshotProvider.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 08.11.20.
//

#if os(iOS)
import UIKit

extension UIViewController: SnapshotProvider {
	public func snapshot() -> Snapshot {
		Snapshot(view)
	}
}
#endif
