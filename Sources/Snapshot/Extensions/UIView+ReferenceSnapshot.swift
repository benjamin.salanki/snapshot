//
//  UIView+ReferenceSnapshot.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 18.10.20.
//

#if os(iOS)
import UIKit
import XCTest

extension UIView {
	/// Creates and stores a reference `Snapshot` of the receiver.
	/// - Parameters:
	///   - testCase: The `XCTestCase` to attach the snapshot to.
	///   - name: The name of the snapshot.
	/// - Returns: A `Snapshot` created from the receiver.
	@discardableResult
	func referenceSnapshot(_ testCase: XCTestCase, name: String) throws -> Snapshot {
		let url = testCase.snapshotURL(name)
		let reference = testCase.takeSnapshot(self, name: "\(name)-reference")
		try FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
		try reference.lightImage.pngData()?.write(to: url.appendingPathComponent("Default.png"))
		try reference.darkImage.pngData()?.write(to: url.appendingPathComponent("Dark.png"))
		try JSONEncoder().encode(Imageset()).write(to: url.appendingPathComponent("Contents.json"))
		return reference
	}
}

// MARK: - Private

/// Struct representing a `Contents.json` used in `.xcassets` folders.
fileprivate struct Imageset: Codable {
	var info = Info()
	var images: [Image] = [.init(filename: "Default.png"),
						   .init(filename: "Dark.png", appearances: [Image.Appearance()])]
	struct Image: Codable {
		var filename: String
		var idiom = "universal"
		var appearances: [Appearance]?
		struct Appearance: Codable {
			var appearance = "luminosity"
			var value = "dark"
		}
	}
	struct Info: Codable {
		var author = "xcode"
		var version = 1
	}
}
#endif
