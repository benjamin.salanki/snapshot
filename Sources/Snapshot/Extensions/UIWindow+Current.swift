//
//  UIWindow+Current.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 05.10.20.
//

#if os(iOS)
import UIKit

public extension UIWindow {
	/// Returns the current key window.
	static func current(on sceneContainer: SceneContainer = UIApplication.shared) -> UIWindow? {
		sceneContainer
			.activeScenes
			.compactMap { $0.delegate as? UIWindowSceneDelegate }
			.first?.window ?? nil
	}
}

// MARK: - Dependency Injection Helpers

public protocol SceneDelegateContainer {
	var delegate: UISceneDelegate? { get }
}

extension UIScene: SceneDelegateContainer {}

public protocol SceneContainer {
	var activeScenes: [SceneDelegateContainer] { get }
}

extension UIApplication: SceneContainer {
	public var activeScenes: [SceneDelegateContainer] {
		Array(connectedScenes)
	}
}
#endif
