//
//  XCTAttachment+Snapshot.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 18.10.20.
//

import XCTest

extension XCTAttachment {
	/// Creates two (light and dark mode) attachments from a `Snapshot`.
	/// - Parameter snapshot: The snapshot to extract the images for the attachment
	/// from.
	/// - Returns: A tuple of `XCTAttachment`s with images of the passed `Snapshot`.
	static func snapshot(_ snapshot: Snapshot) -> (XCTAttachment, XCTAttachment) {
		var attachments: [XCTAttachment] = []
		[(snapshot[keyPath: \.lightImage], "light"),
		 (snapshot[keyPath: \.darkImage], "dark")].forEach { (image, nameModifier) in
			let attachment = XCTAttachment(image: image)
			attachment.name = "\(snapshot.name)-\(nameModifier)"
			attachments.append(attachment)
		}
		return (attachments[0], attachments[1])
	}
}
