//
//  Snapshot.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 11.10.20.
//

#if os(iOS)
import UIKit

/// A screenshot of a `UIView`.
public struct Snapshot {
	/// `UIImage` representation of the snapshot in light mode.
	public let lightImage: UIImage
	/// `UIImage` representation of the snapshot in dark mode.
	public let darkImage: UIImage

	/// The name of the snapshot.
	public var name: String

	// MARK: - Initialization

	/// Returns a newly created `Snapshot` from the passed `UIView`.
	/// - Parameter view: The view to render a snapshot of.
	/// - Parameter name: The name for the snapshot.
	public init(_ view: UIView, name: String = "Default") {
		self.name = name
		(lightImage, darkImage) = view.images()
	}

	/// Returns a newly created `Snapshot` from the passed string.
	///
	/// If an image can not be found for `referenceSnapshotName`, returns `nil`.
	/// - Parameter referenceSnapshotImages: File path for reference snapshot's
	/// `.imageset`.
	public init?(_ referenceSnapshotImages: URL) {
		guard let images = UIImage.referenceSnapshotImages(referenceSnapshotImages) else {
			return nil
		}
		self.name = referenceSnapshotImages.lastPathComponent
		self.lightImage = images.light
		self.darkImage = images.dark
	}

	// MARK: - Difference

	/// Calculates the difference from the receiver and the passed `Snapshot`.
	///
	/// Uses `CIDifferenceBlendMode` filter to calculate the difference.
	/// If the two snapshots are equivalent, the result of this call will also be equivalent with either.
	/// - Parameter snapshot: The other snapshot to calculate the difference from.
	/// - Returns: A snapshot that contains the difference from the receiver's  and the passed
	/// snapshot's `image` property.
	public func difference(from snapshot: Snapshot) -> Snapshot {
		guard self != snapshot else { return self }
		let lightImageDifference = UIImage.snapshotDifference(self, snapshot, keyPath: \.lightImage)
		let darkImageDifference = UIImage.snapshotDifference(self, snapshot, keyPath: \.darkImage)
		return Snapshot(lightImage: lightImageDifference, darkImage: darkImageDifference)
	}
}

// MARK: - Private

extension Snapshot {

	/// Returns a newly created `Snapshot` using the passed `UIImage`s.
	/// - Parameter lightImage: The image to set for the `lightImage` property of the snapshot.
	/// - Parameter darkImage: The image to set for the `darkImage` property of the snapshot.
	/// - Parameter name: The name for the snapshot.
	private init(lightImage: UIImage, darkImage: UIImage, name: String = "Default") {
		self.lightImage = lightImage
		self.darkImage = darkImage
		self.name = name
	}
}

// MARK: - Equatable

extension Snapshot: Equatable {
	public static func == (lhs: Snapshot, rhs: Snapshot) -> Bool {
		let lhsLightData: Data! = lhs.lightImage.pngData()
		let rhsLightData: Data! = rhs.lightImage.pngData()
		let lhsDarkData: Data! = lhs.darkImage.pngData()
		let rhsDarkData: Data! = rhs.darkImage.pngData()
		return lhsLightData.elementsEqual(rhsLightData) && lhsDarkData.elementsEqual(rhsDarkData)
	}
}

extension Snapshot {

	/// Environment variables used by `Snapshot`.
	enum Variables: String, EnvironmentVariable {
		case failOnMissingReference = "snapshot_failOnMissingReference"
		case referencePath = "snapshot_referencePath"
	}

	/// Errors thrown by `Snapshot`.
	enum Errors: Error {
		case referenceMissing(String)
		case snapshotDoesNotMatchReference(String)
	}
}
#endif
