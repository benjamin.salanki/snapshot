//
//  SnapshotProvider.swift
//  TestSuite
//
//  Created by Benjamin Salanki on 11.10.20.
//

import Foundation

#if os(iOS)
/// Protocol describing an object that provides snapshot taking functionality.
public protocol SnapshotProvider {
	/// Creates a snapshot of the receiver's contents.
	func snapshot() -> Snapshot
}
#endif
