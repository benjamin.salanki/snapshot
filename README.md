# Snapshot

`XCTestCase` snapshot testing of `UIView`s in a unit test setting.

## Requirements

Minimum Swift Tools version: 5.3.
Minimum iOS version: 13.

## Snapshot

`Snapshot` offers easy and fast snapshot testing to `XCTestCase`, allowing snapshots to be
created, stored and verified with a single command from any `XCTestCase` unit test.

### Testing Snapshots

It is encouraged to use `Snapshot` in combination with `GivenWhenThen` and the new `expect()`
functionality.

```swift
func testSnapshotEquality() throws {
	try feature("testSnapshotEquality") { [self] in
		try scenario("ViewController Snapshot") {
			try given("a view controller of type `SomeViewController`") { _ in
				SomeViewController()
			}
			.then("its snapshot matches its reference") { viewController in
				try expect(matchesSnapshotReference: viewController)
			}
		}
	}
}
```

The above code, in a default setup, will create a snapshot of the view controller's view and 
match it to the reference snapshot. If there is no reference snapshot provided, the generated
snapshot will be set as the new reference for subsequent runs. 

Snapshots are generated for light and dark mode automatically, and they are generated at the
screen size of the simulator/device the tests are run on. `Snapshot` groups screenshots based
on the device type, so it is possible to create snapshots for different devices.

### Providing Snapshots

`Snapshot` provides the `SnapshotProvider` protocol. Any object conforming to this protocol
can be used in snapshot testing. By default, `UIView`, `UIViewController` and `UIScreen` all 
conform to this protocol and can be used for snapshot testing.

### Configuration

`Snapshot` provides ways to configure its operation.

#### Reference Snapshots

By default, `Snapshot` stores reference snapshots in a temporary location. It is possible to add 
an `.xcassets` folder to your test target and specify it as the location for your reference snapshots.

Doing this will enable you to include your new and modified reference screenshots with your
merge requests and allow the reviewers to do a sanity check of the screenshots before they
are committed to the `develop` or `master` branches.

To enable this, create an `.xcassets` in your test target and specify it's path in the 
`snapshot_referencePath` environment variable. For a top level `.xcassets` folder (named 
`SnapshotReference`) in your testing target (named `Test`) this could be set to 
`$(SRCROOT)/Tests/SnapshotReference.xcassets`

>**Important:** make sure you do **not** include this `.xcassets` folder in your target upon
compilation.

#### Missing Reference

By default, `Snapshot` treats missing reference snapshots as first-run scenarios, creating and
storing a new snapshot for subsequent runs as baselines. This is encouraged to be used on
development machines, as the generated reference snapshots can be automatically added to
merge requests and reviewed before being added to the codebase of the `develop` or `master`
branches.

On CI machines running tests for `release` or `master` branches it is encouraged to fail when
a test encounters a missing reference snapshot, as in those automated situations it could not
be ensured that the snapshots generated would match the expectations. To enable this functionality,
add the environment variable `snapshot_failOnMissingReference` and set its value to `true`.

### Accessing the Current Window

To access the current window from unit tests, use `UIWindow.current()`.
